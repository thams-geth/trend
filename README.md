# Trend

### Github Trending Repositories in Android

An Android App that lists the most trending repositories in Android from githubtrendingapi.
https://githubtrendingapi.docs.apiary.io/


#### App Features
* Users can view the most trending repositories .
* Users can search repositories by repository name .

#### App Architecture 
Based on mvvm architecture and repository pattern.

#### Library Used
    Architecture Components
        ViewModel - Store UI-related data that isn't destroyed on app rotations. Easily schedule asynchronous tasks for optimal execution
        Databinding - Declaratively bind observable data to UI elements
        LiveData - Build data objects that notify views when the underlying database changes
        Navigation - Handle everything needed for in-app navigation
        Room - Access your app's SQLite database with in-app objects and compile-time checks
    Material Design
    Glide - for image loading
    Koin - for dependency injection
    Coroutines - for managing background threads
    Retrofit - for API integration
    SwipeRefreshlayout - for refresh view layout


#### The app includes the following main components:

* A local database that servers as a single source of truth for data presented to the user. 
* A web api service.
* A repository that works with the database and the api service, providing a unified data interface.
* A ViewModel that provides data specific for the UI.
* The UI, which shows a visual representation of the data in the ViewModel.


#### App Packages
* adapter - contains 
    * viewholders and adapter classes 
* data - contains 
    * db - databse and dao classes for local storage
    * remote - api service to fetch data from remote 
    * repository - contains the repository classes, responsible for triggering api requests and saving the response in the database  
* di.module - contains dependency injection classes, using koin
* model - contains model data, entity, response data 
* utils - contains view utils classes,contants , network utils and binding adapter
* view - contains
    * activity - contains activity classes
    * fragments - contains fragments classes

