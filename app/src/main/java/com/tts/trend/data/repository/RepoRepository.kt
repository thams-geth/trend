package com.tts.trend.data.repository

import com.tts.trend.data.db.dao.RepoDao
import com.tts.trend.data.remote.ApiService
import com.tts.trend.model.RepoData


class RepoRepository(private val apiService: ApiService, private val repoDao: RepoDao) : BaseRepository() {


    suspend fun getRepo() = safeApiCall { apiService.getRepo() }

    fun insertAll(repoData: List<RepoData>) = repoDao.insertAll(repoData)

    fun loadAll() = repoDao.loadAll()

    fun deleteAll() = repoDao.deleteAll()


}