package com.tts.trend.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tts.trend.data.db.dao.RepoDao
import com.tts.trend.model.RepoData


@Database(entities = [RepoData::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val repoDao: RepoDao

    companion object {
        const val DB_NAME = "AppDatabase.db"
    }
}
