package com.tts.trend.data.repository

import android.accounts.NetworkErrorException
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.UnknownHostException

abstract class BaseRepository {

    suspend fun <T> safeApiCall(
        apiCall: suspend () -> T
    ): Resource<T> {
        return withContext(Dispatchers.IO) {
            try {
                Resource.Success(apiCall.invoke())
            } catch (e: Exception) {
                Log.e("Exception", e.message.toString())
                when (e) {
                    is NetworkErrorException -> {
                        Resource.Failure(ErrorStatus.NO_CONNECTION)
                    }
                    is UnknownHostException -> {
                        Resource.Failure(ErrorStatus.NO_CONNECTION)
                    }
                    else -> {
                        Resource.Failure(ErrorStatus.UNKNOWN)
                    }
                }
            }
        }
    }

}