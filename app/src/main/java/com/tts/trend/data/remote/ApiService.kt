package com.tts.trend.data.remote

import com.tts.trend.model.RepoData
import com.tts.trend.utils.Constants
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.util.concurrent.TimeUnit

interface ApiService {
    @GET("/repositories")
    suspend fun getRepo(): List<RepoData>

    companion object {
        fun create(): ApiService {
            val loggingInterceptor = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            val okHttpClientBuilder = OkHttpClient.Builder()
                .connectTimeout(Constants.TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(Constants.TIME_OUT, TimeUnit.SECONDS)
                .addNetworkInterceptor(loggingInterceptor)
//                .addInterceptor { chain ->
//                    val url = chain
//                        .request()
//                        .url
//                        .newBuilder()
//                        .addQueryParameter("apikey", Constants.API_KEY)
//                        .build()
//                    val request = chain.request().newBuilder().url(url).build()
//                    chain.proceed(request)
//                }
                .build()

            return Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClientBuilder)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiService::class.java)
        }
    }

}