package com.tts.trend.data.repository

sealed class Resource<out T> {
    data class Success<out T>(val value: T) : Resource<T>()
    data class Failure(val errorStatus: ErrorStatus) : Resource<Nothing>()
    object Loading : Resource<Nothing>()
}

enum class ErrorStatus {
    NO_CONNECTION,
    UNKNOWN,
}
