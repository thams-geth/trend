package com.tts.trend.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.tts.trend.model.RepoData


@Dao
interface RepoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(repoData: List<RepoData>)

    @Query("SELECT * FROM repo")
    fun loadAll(): List<RepoData>

    @Query("DELETE FROM repo")
    fun deleteAll()

//    @Delete
//    fun delete(repoEntity: RepoEntity)


//    @Query("SELECT * FROM repo where name = :queryName")
//    fun loadByName(queryName: String): MutableList<RepoEntity>?
}
