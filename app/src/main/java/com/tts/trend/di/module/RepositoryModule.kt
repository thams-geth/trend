package com.tts.trend.di.module

import com.tts.trend.data.db.dao.RepoDao
import com.tts.trend.data.remote.ApiService
import com.tts.trend.data.repository.RepoRepository


fun provideRepoRemoteRepository(apiService: ApiService, repoDao: RepoDao): RepoRepository {
    return RepoRepository(apiService, repoDao)
}

