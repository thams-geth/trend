package com.tts.trend.di.module

import android.content.Context
import com.tts.trend.data.remote.ApiService
import com.tts.trend.utils.Constants
import com.tts.trend.utils.Constants.Companion.API_KEY
import com.tts.trend.utils.Constants.Companion.TIME_OUT
import com.tts.trend.utils.NetworkUtils
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

fun provideNetworkState(context: Context): NetworkUtils {
    return NetworkUtils(context)

}

fun provideRetrofit(
    okHttpClient: OkHttpClient,
): Retrofit {
    return Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .client(okHttpClient)
        .build()
}

fun provideOkHttpClient(): OkHttpClient {
    val okHttpClientBuilder = OkHttpClient.Builder()
        .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
        .readTimeout(TIME_OUT, TimeUnit.SECONDS)

    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    okHttpClientBuilder.addNetworkInterceptor(loggingInterceptor)
    okHttpClientBuilder.addInterceptor { chain ->

        val url = chain
            .request()
            .url
            .newBuilder()
            .addQueryParameter("apikey", API_KEY)
            .build()
        val request = chain.request().newBuilder().url(url).build()


        chain.proceed(request)
    }.build()

    return okHttpClientBuilder.build()
}

fun provideApi(retrofit: Retrofit): ApiService {
    return retrofit.create(ApiService::class.java)
}





