package com.tts.trend.di.module

import android.app.Application
import androidx.room.Room
import com.tts.trend.data.db.AppDatabase
import com.tts.trend.data.db.dao.RepoDao


fun provideDatabase(application: Application): AppDatabase {
    return Room.databaseBuilder(
        application,
        AppDatabase::class.java,
        AppDatabase.DB_NAME
    ).build()
}

fun provideRepoDao(database: AppDatabase): RepoDao {
    return database.repoDao
}
