package com.tts.trend.di.module

import com.tts.trend.data.remote.ApiService
import com.tts.trend.viewmodel.RepoListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { RepoListViewModel(get(), get()) }
}

val repositoryModule = module {
    single { provideRepoRemoteRepository(get(), get()) }
}

val databaseModule = module {
    single { provideDatabase(get()) }
    single { provideRepoDao(get()) }
}

val networkModule = module {
//    single { provideOkHttpClient() }
//    single { provideRetrofit(get()) }
//    single { provideApi(get()) }
    single { ApiService.create() }
    single { provideNetworkState(get()) }
}



