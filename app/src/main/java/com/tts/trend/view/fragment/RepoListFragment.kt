package com.tts.trend.view.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.tts.trend.R
import com.tts.trend.data.repository.ErrorStatus
import com.tts.trend.data.repository.Resource
import com.tts.trend.databinding.FragmentRepoListBinding
import com.tts.trend.model.RepoData
import com.tts.trend.utils.handleApiError
import com.tts.trend.utils.visible
import com.tts.trend.viewmodel.RepoListViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class RepoListFragment : Fragment() {

    private val mViewModel: RepoListViewModel by viewModel()
    private lateinit var binding: FragmentRepoListBinding
    private lateinit var mContext: Context
    lateinit var dataList: ArrayList<RepoData>
    private lateinit var searchView: SearchView


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_repo_list, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hideAllView()
        binding.viewModel = mViewModel
        dataList = ArrayList()

        mViewModel.loadRepoList()
        mViewModel.repo.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    Log.d(TAG, "Loading")
                    binding.progressBar.visible(true)
                    binding.swipeContainer.isRefreshing = true
                }
                is Resource.Success -> {
                    Log.d(TAG, "Success")
                    onSuccess(it)

                }
                is Resource.Failure -> {
                    Log.e(TAG, "Failure")
                    onFailure(it)
                }
            }
        })

        mViewModel.selectedRepo.observe(viewLifecycleOwner, {
            //If user click the list , send the date and display the details
//            findNavController().navigate(RepoListFragmentDirections.actionRepoListFragmentToRepoDetailsFragment(it))
        })
        binding.btnTryAgain.setOnClickListener {
            retry()
        }

    }

    private fun onSuccess(it: Resource.Success<List<RepoData>>) {
        showSuccessView()
        dataList.clear()
        var i = 0
        for (data in it.value) {
            data.id = i++
            dataList.add(data)
        }
        mViewModel.updateList(dataList)
        mViewModel.insertAllIntoLocalDatabase(dataList)
    }

    private fun onFailure(it: Resource.Failure) {
        showFailureView()
        if (it.errorStatus == ErrorStatus.NO_CONNECTION) {
            binding.txtErrorMsg.text = getString(R.string.no_network_connection)
            handleApiError(it) {
                retry()
            }
        } else {
            binding.txtErrorMsg.text = getString(R.string.something_went_wrong)
            handleApiError(it)
        }
    }

    private fun retry() {
        mViewModel.loadRepoList()
        hideAllView()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_main, menu)
        val searchItem: MenuItem = menu.findItem(R.id.action_search)
        searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(search: String?): Boolean {
                if (search != null && search != "")
                    mViewModel.search(search, dataList)
                else {
                    mViewModel.updateList(dataList)
                }
                return false
            }
        })

        return super.onCreateOptionsMenu(menu, inflater)

    }


    private fun showSuccessView() {
        binding.progressBar.visible(false)
        binding.swipeContainer.isRefreshing = false
        binding.swipeContainer.visible(true)
        binding.clNoInternet.visible(false)
    }

    private fun showFailureView() {
        binding.progressBar.visible(false)
        binding.swipeContainer.isRefreshing = false
        binding.swipeContainer.visible(false)
        binding.clNoInternet.visible(true)
    }

    private fun hideAllView() {
        binding.swipeContainer.visible(false)
        binding.clNoInternet.visible(false)
    }


    companion object {
        val TAG = RepoListFragment::class.simpleName
    }
}