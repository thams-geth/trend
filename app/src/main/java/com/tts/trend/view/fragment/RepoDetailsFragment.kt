package com.tts.trend.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.tts.trend.R
import com.tts.trend.databinding.FragmentRepoDetailsBinding

class RepoDetailsFragment : Fragment() {

    private lateinit var binding: FragmentRepoDetailsBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_repo_details, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val args = RepoDetailsFragmentArgs.fromBundle(requireArguments())
//        binding.data = args.RepoDetails
    }
}