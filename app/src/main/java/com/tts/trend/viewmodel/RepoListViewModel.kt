package com.tts.trend.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tts.trend.adapter.BaseClickListener
import com.tts.trend.adapter.RepoAdapter
import com.tts.trend.data.repository.ErrorStatus
import com.tts.trend.data.repository.RepoRepository
import com.tts.trend.data.repository.Resource
import com.tts.trend.model.RepoData
import com.tts.trend.utils.NetworkUtils
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import kotlin.collections.ArrayList

class RepoListViewModel(private val repoRepository: RepoRepository, private val networkUtils: NetworkUtils) : ViewModel() {

    private var _selectedRepo: MutableLiveData<RepoData> = MutableLiveData<RepoData>()
    val selectedRepo: LiveData<RepoData>
        get() = _selectedRepo

    private val listener = object : BaseClickListener<RepoData> {
        override fun onClickItem(data: RepoData, position: Int) {
            _selectedRepo.value = data
        }
    }
    private var dataList: ArrayList<RepoData> = ArrayList()
    var adapter: RepoAdapter = RepoAdapter(dataList, listener)


    private var _repo: MutableLiveData<Resource<List<RepoData>>> = MutableLiveData<Resource<List<RepoData>>>()
    val repo: LiveData<Resource<List<RepoData>>>
        get() = _repo


    fun loadRepoList() {
        val a = networkUtils.getNetworkLiveData()
        if (a.value == true) {
            viewModelScope.launch {
                _repo.value = Resource.Loading
                _repo.value = repoRepository.getRepo()
            }
        } else {
            loadAllFromLocalDatabase()
        }
    }

    fun insertAllIntoLocalDatabase(list: ArrayList<RepoData>) {
        CoroutineScope(Dispatchers.IO).launch {
            repoRepository.insertAll(list)
        }
    }

    private fun deleteAllFromLocalDatabase() {
        CoroutineScope(Dispatchers.IO).launch {
            repoRepository.deleteAll()
        }
    }

    private fun loadAllFromLocalDatabase() {
        CoroutineScope(Dispatchers.IO).launch {
            _repo.postValue(Resource.Loading)
            val list = repoRepository.loadAll()
            if (list.isNotEmpty())
                _repo.postValue(Resource.Success(list))
            else
                _repo.postValue(Resource.Failure(ErrorStatus.NO_CONNECTION))

        }
    }

    fun updateList(list: ArrayList<RepoData>?) {
        dataList.clear()
        if (list != null) {
            dataList.addAll(list)
            adapter.notifyDataSetChanged()
        }
    }

    fun search(text: String, list: ArrayList<RepoData>?) {
        val temp: ArrayList<RepoData> = ArrayList()
        temp.clear()
        if (list != null) {
            for (data in list) {
                if (data.name.toString().toLowerCase(Locale.ENGLISH).contains(text.toLowerCase(Locale.ENGLISH))) {
                    temp.add(data)
                }
            }
        }
        updateList(temp)
    }


}