package com.tts.trend.utils

import android.net.Uri
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.core.graphics.toColorInt
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.tts.trend.R


@BindingAdapter("colorCode")
fun loadColorCode(view: androidx.appcompat.widget.AppCompatImageView, url: String) {
    val drawable = ContextCompat.getDrawable(view.context, R.drawable.ic_circle)
    drawable?.setTint(url.toColorInt())
//    binding.itemImgLanguage.setImageDrawable(drawable)
    view.setImageDrawable(drawable)
}


@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String) {
    Glide.with(view.context)
        .load(Uri.parse(url))
        .into(view)
}


