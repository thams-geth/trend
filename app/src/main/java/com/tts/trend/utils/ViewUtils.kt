package com.tts.trend.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.tts.trend.R
import com.tts.trend.data.repository.ErrorStatus
import com.tts.trend.data.repository.Resource


fun toast(context: Context, msg: String) {
    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
}

fun View.visible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun View.snackbar(message: String, action: (() -> Unit)? = null) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    action?.let {
        snackbar.setAction("Retry") {
            it()
        }
    }
    snackbar.show()
}

fun Fragment.handleApiError(
    failure: Resource.Failure,
    retry: (() -> Unit)? = null
) {
    when (failure.errorStatus) {
        ErrorStatus.NO_CONNECTION -> requireView().snackbar(getString(R.string.no_network_connection), retry)
        ErrorStatus.UNKNOWN -> requireView().snackbar(getString(R.string.something_went_wrong))
    }
}