package com.tts.trend

import android.app.Application
import com.tts.trend.di.module.databaseModule
import com.tts.trend.di.module.networkModule
import com.tts.trend.di.module.repositoryModule
import com.tts.trend.di.module.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
//            androidLogger()
            androidContext(this@App)
            modules(listOf(viewModelModule, repositoryModule, networkModule, databaseModule))
        }
    }
}