package com.tts.trend.adapter.viewholder

import androidx.recyclerview.widget.RecyclerView
import com.tts.trend.adapter.BaseClickListener
import com.tts.trend.databinding.ItemRepoBinding
import com.tts.trend.model.RepoData

class RepoViewHolder(private val viewBinding: ItemRepoBinding, private val listener: BaseClickListener<RepoData>) : RecyclerView.ViewHolder(viewBinding.root) {
    fun setModelDate(data: RepoData) {
        viewBinding.data = data
        viewBinding.position = adapterPosition
        viewBinding.listener = listener
    }
}