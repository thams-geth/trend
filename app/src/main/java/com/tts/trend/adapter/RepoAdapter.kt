package com.tts.trend.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tts.trend.adapter.viewholder.RepoViewHolder
import com.tts.trend.databinding.ItemRepoBinding
import com.tts.trend.model.RepoData

class RepoAdapter(
    private val list: ArrayList<RepoData>,
    private val listener: BaseClickListener<RepoData>
) : RecyclerView.Adapter<RepoViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoViewHolder {
        val binding = ItemRepoBinding.inflate(LayoutInflater.from(parent.context))
        return RepoViewHolder(binding,listener)
    }

    override fun onBindViewHolder(holder: RepoViewHolder, position: Int) {
        holder.setModelDate(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }


}