package com.tts.trend.adapter

interface BaseClickListener<T> {
    fun onClickItem(data: T, position: Int)

}